# updates

Displays the number of pending updates of running
[Incus](https://linuxcontainers.org/incus/) containers.

Depends on `incus`.

