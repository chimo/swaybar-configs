#!/bin/sh -eu

# TODO: split os-specific things into their own files

get_container_version() (
    container="${1}"
    cmd="grep 'VERSION_ID=' /etc/os-release | grep -oE '[0-9]+\.[0-9]+'"

    container_os_version=$(
        incus exec -n "${container}" -- sh -c "${cmd}"
    )

    echo "${container_os_version}"
)


check_dist_upgrade() (
    container="${1}"
    latest_alpine_version="${2}"

    container_os_version=$(get_container_version "${container}")

    echo "${latest_alpine_version} > ${container_os_version}" | bc
)


_apk() (
    container="${1}"

    cmd="apk -q update && apk list -u | wc -l"

    _exec "${container}" "${cmd}"
)


get_latest_alpine_version() (
    # https://stackoverflow.com/a/12704727
    git -c 'versionsort.suffix=-' \
        ls-remote --exit-code --refs --sort='version:refname' \
        --tags https://git.alpinelinux.org/aports '*.*.*' \
        | grep -vE '[0-9]+\.[0-9]+_rc' \
        | tail -n 1 \
        | cut -d '/' -f3 \
        | grep -oE '[0-9]+\.[0-9]+'
)


_apt() (
    # TODO: finish this if/when I have apt-based containers
    echo "apt: To be implemented..."
)


_exec() (
    container="${1}"
    cmd="${2}"

    incus exec -n "${container}" -- sh -c "${cmd}"
)


check_for_updates() (
    containers=$(incus list status=running -c n,config:image.os --format csv)
    details=""
    latest_alpine_version=""
    total_containers=0
    total_dist_upgrades=0
    total_updates=0

    while IFS= read -r line
    do
        nb_updates=0

        # `line` is "container-name,container-os", lowercased
        # `name` is everything before the comma
        # `os` is everything after the comma
        line=$(echo "${line}" | tr '[:upper:]' '[:lower:]')
        name="${line%,*}"
        os="${line#*,}"

        case "${os}" in
            "alpine"|"alpinelinux")
                if [ "${latest_alpine_version}" = "" ]; then
                    latest_alpine_version=$(get_latest_alpine_version)
                fi

                needs_dist_upgrade=$(
                    check_dist_upgrade "${name}" "${latest_alpine_version}"
                )

                nb_updates=$(_apk "${name}" "${latest_alpine_version}")
                ;;
            "debian")
                nb_updates=$(_apt "${name}")
                ;;
            *)
                echo "Unknown OS: ${os}"
                ;;
        esac

        details="${details}\n${name} ${nb_updates} ${needs_dist_upgrade}"

        if [ "${nb_updates}" -gt 0 ]; then
            total_containers=$((total_containers + 1))
        fi

        total_dist_upgrades=$((total_dist_upgrades + needs_dist_upgrade))
        total_updates=$((total_updates + nb_updates))
    done<<EOF
$containers
EOF


    if [ "${total_updates}" -gt 0 ]; then
        if [ -z "${UPDATE_LABEL-}" ]; then
            UPDATE_LABEL="\xEF\x81\xA2"
        fi

        if [ -z "${CONTAINER_LABEL-}" ]; then
            CONTAINER_LABEL="\xEF\x91\xA6"
        fi

        printf "%b %s, %b %s" \
            "${UPDATE_LABEL}" "${total_updates}" \
            "${CONTAINER_LABEL}" "${total_containers}"
    fi

    if [ "${total_dist_upgrades}" -gt 0 ]; then
        if [ -z "${DIST_UPGRADE_LABEL-}" ]; then
            DIST_UPGRADE_LABEL="\xEF\x91\xA6"
        fi

        printf "%b %s" "${DIST_UPGRADE_LABEL}" "${total_dist_upgrades}"
    fi

    if [ "${total_updates}" -gt 0 ] || [ "${total_dist_upgrades}" -gt 0 ]; then
        printf "\n%b" "${details}"
    fi

    printf "%b" "\n"
)


main() (
    action="${1-}"
    terminal="${SHBR_TERM:-foot}"

    if [ "${action}" = "--click" ]; then
        script_dir=$(dirname -- "$(readlink -f -- "$0";)")
        states_dir="${script_dir}/../../states"
        "${terminal}" -- sh -c "tail -n +3 ${states_dir}/container-updates.state; read -r -s"
    else
        check_for_updates
    fi
)


main "${@}"

