#!/bin/sh -eu

get_battery_info() (
    battery_uevent="${1}"
    bat_info=$(cat "${battery_uevent}")
    bat_state=$(echo "$bat_info" | grep STATUS | cut -d= -f2)
    bat_full=$(echo "$bat_info" | grep POWER_SUPPLY_CHARGE_FULL= | cut -d= -f2)
    bat_now=$(echo "$bat_info" | grep POWER_SUPPLY_CHARGE_NOW | cut -d= -f2)
    bat_percent=$(echo "scale=2; $bat_now/$bat_full*100" | bc | cut -d. -f1)

    if [ -z "${CHARGING_LABEL-}" ]; then
        CHARGING_LABEL="\xEF\x83\xA7"
    fi

    if [ -z "${DISCHARGING_LABEL-}" ]; then
        DISCHARGING_LABEL="\xEF\x89\x81"
    fi

    if [ "${bat_state}" = "Discharging" ]; then
        label="${DISCHARGING_LABEL}"
    else
        label="${CHARGING_LABEL}"
    fi

    printf "%b %s%%" "${label}" "${bat_percent}"
)

main() (
    if [ -z "${BATTERY_UEVENT-}" ]; then
        BATTERY_UEVENT="/sys/class/power_supply/BAT0/uevent"
    fi

    battery_uevent="${BATTERY_UEVENT}"
    get_battery_info "${battery_uevent}"
)

main "${@}"

