#!/bin/sh -eu

main() (
    action="${1-}"
    terminal="${SHBR_TERM:-foot}"

    if [ "${action}" = "--click" ]; then
        script_dir=$(dirname -- "$(readlink -f -- "$0";)")
        states_dir="${script_dir}/../../states"
        "${terminal}" -- sh -c "tail -n +3 ${states_dir}/stale-apkbuilds.state; read -r -s"
    else
        pkgs=$(~/devel/apkbuilds/check-stale-apkbuilds.sh)
        nb_pkgs=0

        if [ -n "${pkgs}" ]; then
            nb_pkgs=$(echo "${pkgs}" | wc -l)
        fi

        if [ "${nb_pkgs}" -gt 0 ]; then
            # Summary
            printf "A: %d\n" "${nb_pkgs}"

            # Details
            printf '\n%s' "${pkgs}"
        else
            echo ""
        fi
    fi
)

main "${@}"

