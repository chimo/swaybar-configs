#!/bin/sh -eu

action="${1-}"
terminal="${SHBR_TERM:-foot}"

if [ "${action}" = "--click" ]; then
    "${terminal}" -- sh -c 'khal.sh calendar today; read -r -s'
else
    events=$(khal.sh list -a personal now tomorrow -df "" | wc -l)
    bdays=$(khal.sh list -a contact_birthdays today today -df "" | wc -l)

    if [ -z "${EVENT_LABEL-}" ]; then
        EVENT_LABEL="\xEF\x81\xB3"
    fi

    if [ -z "${BDAY_LABEL-}" ]; then
        BDAY_LABEL="\xEF\x87\xBD"
    fi

    if [ "${events}" -gt 0 ]; then
        printf "%b %s" "${EVENT_LABEL}" "${events}"

        # Delimiter between events and birthdays when both have data
        if [ "${bdays}" -gt 0 ]; then
            printf " "
        fi
    fi

    if [ "${bdays}" -gt 0 ]; then
        printf "%b %s" "${BDAY_LABEL}" "${bdays}"
    fi
fi

