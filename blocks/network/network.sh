#!/bin/sh -eu

# TODO: Some more testing is required to handle multiple scenarios
#       (disconnected, wired vs. wifi, multiple interfaces, etc.)

# https://stackoverflow.com/a/15798024
get_wlan_quality() (
    interface_name="${1}"
    signal=$(
        iw dev "${interface_name}" station dump \
            | grep 'signal:' \
            | cut -d "" -f3 \
            | cut -d " " -f1
    )

    if [ "${signal}" -le -100 ]; then
        quality="0"
    elif [ "${signal}" -ge -50 ]; then
        quality="100"
    else
        quality=$(echo "2 * (${signal} + 100)" | bc)
    fi

    echo "${quality}"
)


get_interface_name() (
    # Get the default device
    ip r | grep default | awk '/default/ {print $5}'
)


is_interface_wireless() (
    interface_name="${1}"
    is_wireless=0

    if [ -d "/sys/class/net/${interface_name}/wireless" ]; then
        is_wireless=1
    fi

    echo "${is_wireless}"
)


# Print all variables in space-separated format. If a variable is an empty
# string, it is skipped as to prevent double spaces.
_print() (
    label="${1}"
    shift

    if [ -n "${label}" ]; then
        printf '%b' "${label}"
    fi

    for i in "${@}"; do
        if [ -n "${i}" ]; then
            printf ' %s' "${i}"
        fi
    done
)


main() (
    interface_name=$(get_interface_name)
    is_wireless=$(is_interface_wireless "${interface_name}")

    lan_ip=""
    wan_ip=""
    wlan_ssid=""
    wlan_quality=""

    if [ "${is_wireless}" -eq 1 ]; then
        if [ -z "${WIFI_GOOD_LABEL-}" ]; then
            WIFI_EXCELLENT_LABEL="\xEF\x87\xAB"
        fi

        if [ -z "${WIFI_MEDIUM_LABEL-}" ]; then
            WIFI_GOOD_LABEL="\xEF\x87\xAB"
        fi

        if [ -z "${WIFI_BAD_LABEL-}" ]; then
            WIFI_BAD_LABEL="\xEF\x87\xAB"
        fi

        wlan_ssid=$(
            iw dev "${interface_name}" link \
                | grep SSID \
                | cut -d " " -f2
        )

        wlan_quality=$(get_wlan_quality "${interface_name}")

        if [ "${wlan_quality}" -gt 65 ]; then
            WIFI_LABEL="${WIFI_GOOD_LABEL}"
        elif [ "${wlan_quality}" -gt 35 ]; then
            WIFI_LABEL="${WIFI_MEDIUM_LABEL}"
        else
            WIFI_LABEL="${WIFI_BAD_LABEL}"
        fi

        if [ -n "${wlan_ssid}" ]; then
            wlan_ssid="(${wlan_ssid})"
        fi
    fi

    lan_ip=$(
        ip -br -4 addr list dev "${interface_name}" \
            | awk '{print $3}' \
            | cut -d/ -f1
    )

    if [ -n "${IP_ENDPOINT}" ]; then
        wan_ip=$(wget -O- -q "${IP_ENDPOINT}")

        if [ -n "${wan_ip}" ]; then
            wan_ip="/ ${wan_ip}"
        fi
    fi

    _print "${WIFI_LABEL}" "${wlan_quality}%" "${wlan_ssid}" "${lan_ip}" \
        "${wan_ip}"
)


main

