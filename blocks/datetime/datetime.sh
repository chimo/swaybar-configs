#!/bin/sh -eu

action="${1-}"
terminal="${SHBR_TERM:-foot}"

if [ -z "${LABEL-}" ]; then
    LABEL="\xEF\x80\x97"
fi

if [ "${action}" = "--click" ]; then
    "${terminal}" -- sh -c 'cal -y; read -r -s'
else
    printf "%b %s" "${LABEL}" "$(date +'%Y-%m-%d %H:%M:%S %Z')"
fi

