#!/bin/sh -eu

##
# Vars.
##

# CO2
CO2_LOW_LABEL="${CO2_LOW_LABEL:-<span color='#C1E1C1'>C:</span>}"
CO2_MEDIUM="${CO2_MEDIUM:-800}"
CO2_MEDIUM_LABEL="${CO2_MEDIUM_LABEL:-<span color='#FFFAA0'>C:</span>}"
CO2_HIGH="${CO2_HIGH:-1000}"
CO2_HIGH_LABEL="${CO2_HIGH_LABEL:-<span color='#FAA0A0'>C:</span>}"

# Radon
RADON_LOW_LABEL="${RADON_LOW_LABEL:-<span color='#C1E1C1'>\xEF\x9E\xB9</span>}"
RADON_MEDIUM="${RADON_MEDIUM:-150}"
RADON_MEDIUM_LABEL="${RADON_MEDIUM_LABEL:-<span color='#FFFAA0'>\xEF\x9E\xB9</span>}"
RADON_HIGH="${RADON_HIGH:-200}"
RADON_HIGH_LABEL="${RADON_HIGH_LABEL:-<span color='#FAA0A0'>\xEF\x9E\xB9</span>}"

# VOC
VOC_LOW_LABEL="${VOC_LOW_LABEL:-<span color='#C1E1C1'>V:</span>}"
VOC_MEDIUM="${VOC_MEDIUM:-250}"
VOC_MEDIUM_LABEL="${VOC_MEDIUM_LABEL:-<span color='#FFFAA0'>V:</span>}"
VOC_HIGH="${VOC_HIGH:-2000}"
VOC_HIGH_LABEL="${VOC_HIGH_LABEL:-<span color='#FAA0A0'>V:</span>}"


extract_value() (
    json="${1}"
    field="${2}"

    value=$(extract "${json}" "${field}" | awk '{print $1}')

    # Remove decimals
    echo "${value}" / 1 | bc
)


extract() (
    json="${1}"
    field="${2}"

    printf '%s' "${json}" | cut -d, -f "${field}" | cut -d: -f2 | tr -d '"'
)


get_radon_label() (
    value="${1}"

    if [ "${value}" -ge "${RADON_HIGH}" ]; then
        label="${RADON_HIGH_LABEL}"
    elif [ "${value}" -ge "${RADON_MEDIUM}" ]; then
        label="${RADON_MEDIUM_LABEL}"
    else
        label="${RADON_LOW_LABEL}"
    fi

    echo "${label}"
)


get_co2_label() (
    value="${1}"

    if [ "${value}" -ge "${CO2_HIGH}" ]; then
        label="${CO2_HIGH_LABEL}"
    elif [ "${value}" -ge "${CO2_MEDIUM}" ]; then
        label="${CO2_MEDIUM_LABEL}"
    else
        label="${CO2_LOW_LABEL}"
    fi

    echo "${label}"
)


get_voc_label() (
    value="${1}"

    if [ "${value}" -ge "${VOC_HIGH}" ]; then
        label="${VOC_HIGH_LABEL}"
    elif [ "${value}" -ge "${VOC_MEDIUM}" ]; then
        label="${VOC_MEDIUM_LABEL}"
    else
        label="${VOC_LOW_LABEL}"
    fi

    echo "${label}"
)


get_data() (
    json=$(
        wget -O- --header "Secret: ${API_SECRET}" \
            "${API_ENDPOINT}" -q
    )

    echo "${json}"
)


handle_click() (
    script_dir=$(dirname -- "$(readlink -f -- "${0}";)")
    json=$(tail -n 1 "${script_dir}"/../../states/airthings.state)

    humidity=$(extract "${json}" 1 | tr -d ' ')
    radon_st=$(extract "${json}" 2 | tr -d ' ')
    radon_lt=$(extract "${json}" 3 | tr -d ' ')
    temperature=$(extract "${json}" 4 | tr -d ' ')
    pressure=$(extract "${json}" 5 | tr -d ' ')
    co2=$(extract "${json}" 6 | tr -d ' ')
    voc=$(extract "${json}" 7 | tr -d ' ')
    epoch=$(extract "${json}" 8 | tr -d '}' | tr -d ' ')
    datetime=$(date -d @"${epoch%.*}")

    out=$(
        printf "%s\n" \
            "Humidity: ${humidity}" \
            "Radon Short Term: ${radon_st}" \
            "Radon Long Term: ${radon_lt}" \
            "Temperature: ${temperature}" \
            "Pressure: ${pressure}" \
            "CO2: ${co2}" \
            "VOC: ${voc}" \
            "Time: ${datetime}"
    )

    "${terminal}" -- sh -c "echo \"${out}\"; read -r -s"

)


main() (
    action="${1-}"
    terminal="${SHBR_TERM:-foot}"

    if [ "${action}" = "--click" ] ; then
        handle_click
    else
        json=$(get_data)

        # Humidity
        humidity=$(extract_value "${json}" 1)
        humidity_label="\xEF\x81\x83" # TODO: configurable

        # Temperature
        temperature=$(extract_value "${json}" 4)
        temperature_label="\xEF\x80\x95" # TODO: configurable

        # Radon
        radon=$(extract_value "${json}" 2)
        radon_label=$(get_radon_label "${radon}")

        co2=$(extract_value "${json}" 6)

        # Only display CO2 levels if above medium threshold
        if [ "${co2}" -ge "${CO2_MEDIUM}" ]; then
            co2_label=$(get_co2_label "${co2}")
        else
            co2=""
        fi

        voc=$(extract_value "${json}" 7)

        # Only display VOC levels if above medium threshold
        if [ "${voc}" -ge "${VOC_MEDIUM}" ]; then
            voc_label=$(get_voc_label "${voc}")
        else
            voc=""
        fi

        divider=""

        if [ -n "${temperature}" ]; then
            printf "%b %s" "${temperature_label}" "${temperature}°"
            divider=" "
        fi

        if [ -n "${humidity}" ]; then
            printf "%s%b %s%%" "${divider}" "${humidity_label}" "${humidity}"
            divider=" "
        fi

        if [ -n "${radon}" ]; then
            printf "%s%b %s" "${divider}" "${radon_label}" "${radon}"
            divider=" "
        fi

        if [ -n "${co2}" ]; then
            printf "%s%b %s" "${divider}" "${co2_label}" "${co2}"
            divider=" "
        fi

        if [ -n "${voc}" ]; then
            printf "%s%b %s" "${divider}" "${voc_label}" "${voc}"
            divider=" "
        fi

        # Full details (not used by swaybar).
        printf "\n\n%s" "${json}"
    fi
)


main "${@}"

