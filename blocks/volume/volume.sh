#!/bin/sh -eu

get_from_wireplumber() (
    if ! output=$(wpctl get-volume @DEFAULT_AUDIO_SINK@); then
        return 1
    fi

    # Volume
    vol=$(echo "${output}" | cut -d " " -f2)
    vol_percent=$(echo "${vol} * 100" | bc | cut -d "." -f1)

    # Mute state
    muted=$(echo "${output}" | cut -d " " -f3)

    if [ "${muted}" = "[MUTED]" ]; then
        if [ -z "${MUTE_LABEL-}" ]; then
            MUTE_LABEL="\xEF\x9A\xA9"
        fi

        state="${MUTE_LABEL}"
    else
        if [ -z "${UNMUTE_LABEL-}" ]; then
            UNMUTE_LABEL="\xEF\x80\xA8"
        fi

        state="${UNMUTE_LABEL}"
    fi

    printf '%b %s%%' "${state}" "${vol_percent}"
)

handle_click() (
    script_dir=$(dirname -- "$( readlink -f -- "$0"; )")
    states_dir="${script_dir}/../../states"
    
    wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle

    get_from_wireplumber > "${states_dir}/volume.state"
)

main() (
    action="${1-}"

    if [ "${action}" = "--click" ]; then
        handle_click
    else
        get_from_wireplumber
    fi
)


main "${@}"

