#!/bin/sh -eu

main() (
    action="${1-}"
    terminal="${SHBR_TERM:-foot}"

    if [ "${action}" = "--click" ]; then
        script_dir=$(dirname -- "$(readlink -f -- "$0";)")
        states_dir="${script_dir}/../../states"
        "${terminal}" -- sh -c "tail -n +3 ${states_dir}/updates.state; read -r -s"
    else
        updates=$(apk list -u)
        nb_updates=0

        if [ -n "${updates}" ]; then
            nb_updates=$(echo "${updates}" | wc -l)
        fi

        if [ "${nb_updates}" -gt 0 ]; then
            if [ -z "${LABEL-}" ]; then
                LABEL="\xEF\x84\x89"
            fi

            # Summary
            printf "%b %d\n" "${LABEL}" "${nb_updates}"

            # Details
            printf "\n%s" "${updates}"
        else
            echo ""
        fi
    fi
)

main "${@}"

