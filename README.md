# shuibar

A `sh`-driven [swaybar](https://swaywm.org/).

I'm using this project as an excuse to learn things. As such, it's probably a
little bit of a mess and subject to change at any time.

One of the goals is to mostly use the tools available on a fresh Alpine Linux
install (Busybox) and keep the package requirements low.

A decent number of "blocks" rely on external services (location, weather, etc.).
Each block should have its own README with more information. Blocks aim to be
somewhat configurable (ex: custom endpoints for external services).

## Usage

See the "config.example" file to configure which blocks to run.

In your sway config file (usually ~/.config/sway/config) point the
"status_command" property to main.sh, with the --json parameter like so:

```
bar {
    status_command '~/.config/sway/swaybar/bin/main.sh -p json'
}
```

