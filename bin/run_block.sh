#!/bin/sh -eu

# Define some paths
bin_dir=$(dirname -- "$( readlink -f -- "$0"; )")
blocks_dir="${bin_dir}/../blocks"
states_dir="${bin_dir}/../states"

mkdir -p "${states_dir}"

usage() (
    printf "%s\n" "Usage: ${0} -b <block> [-c <cooldown>] [-p <json|plain>]

options:
-b      the block to run (ex: datetime.sh).
-c      cooldown value in seconds. Defaults to block's config value.
-p      protocol (\"json\" or \"plain\"). Defaults to \"json\"." 1>&2

    exit 1
)


# https://unix.stackexchange.com/a/598047
is_integer()
(
    case "${1#[+-]}" in
        (*[!0123456789]*) return 1 ;;
        ('')              return 1 ;;
        (*)               return 0 ;;
    esac
)


argparse() (
    block=""
    cooldown=""
    protocol="json"

    while getopts ':b:c:p:' opt; do
        case $opt in
            b)
                block="${OPTARG}"
                ;;
            c)
                cooldown="${OPTARG}"
                ;;
            p)
                protocol="${OPTARG}"

                [ "${protocol}" = "json" ] \
                    || [ "${protocol}" = "plain" ] \
                    || usage
                ;;
            *)
                usage
                ;;
        esac
    done

    shift "$((OPTIND - 1))"

    if [ "${block}" = "" ]; then
        usage
    fi

    main "${block}" "${protocol}" "${cooldown}"
)


run() (
    filename="${1}"
    cooldown="${2}"
    protocol="${3}"

    script_dirname=$(basename "${filename}" ".sh")
    script_dir="${blocks_dir}/${script_dirname}"
    script="${script_dir}/${filename}"
    envfile="${script_dir}/.env"
    statefile="${states_dir}/${filename%.*}".state

    if [ ! -e "${script}" ]; then
        echo "${script}: No such file"
        return 1
    fi

    if [ "${cooldown}" -eq 0 ]; then
        out=$(${script})
    elif [ "${cooldown}" -eq -1 ]; then
        # Cache-busting implies running in the foreground
        if [ -e "${envfile}" ]; then
            . "${envfile}"
        fi

        out=$(${script} | tee "${statefile}")
    else
        # Get last executed time
        if [ -f "${statefile}" ]; then
            last_run=$(stat -c %Y "${statefile}")
        else
            echo "" > "${statefile}"
            last_run=0
        fi

        next_run=$((last_run + cooldown))
        now=$(date +'%s')

        out=$(head -n 1 "${statefile}")

        # If the cooldown has expired, run the command so we get updated data
        # on the next run
        if [ "${now}" -gt "${next_run}" ]; then

            (
                if [ -e "${envfile}" ]; then
                    . "${envfile}"
                fi

                ${script} > "${statefile}"
            )&
        fi
    fi

    if [ -n "${out-}" ]; then
        if [ "${protocol}" = "plain" ]; then
            echo "${out}"
        else
            printf '{"markup": "pango", "full_text": "%s", "name": "%s", "instance": "%s"}\n' \
                "${out}" "${filename}" "${filename}"
        fi
    fi
)


get_cooldown() (
    # Remove file extension from filename to get block name.
    target_block="${1%.*}"
    config_file="${bin_dir}/../config"

    while read -r line
    do
        block="${line%=*}"

        if [ "${block}" = "${target_block}" ]; then
            cooldown="${line#*=}"

            if ! is_integer "${cooldown}"; then
                cooldown=3600
            fi

            echo "${cooldown}"
            break
        fi
    done < "${config_file}"
)


main() (
    block="${1}"
    protocol="${2}"
    cooldown="${3}"

    # Get the block's configured cooldown value if one isn't provided
    if [ -z "${cooldown}" ]; then
        cooldown=$(get_cooldown "${block}")
    fi

    run "${block}" "${cooldown}" "${protocol}"
)


argparse "${@}"

