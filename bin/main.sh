#!/bin/sh -eu

# Define some paths
bin_dir=$(dirname -- "$( readlink -f -- "$0"; )")
blocks_dir="${bin_dir}/../blocks"
states_dir="${bin_dir}/../states"

mkdir -p "${states_dir}"

# https://unix.stackexchange.com/a/598047
is_integer()
(
    case "${1#[+-]}" in
        (*[!0123456789]*) return 1 ;;
        ('')              return 1 ;;
        (*)               return 0 ;;
    esac
)


# Returns success/true (0) if line starts with "#"
# Returns failure/false (1) otherwise
is_comment() (
    line="${1}"

    case "${line}" in
        "#"*) return 0 ;;
    esac

    return 1
)


run() (
    blocks="${1}"
    protocol="${2}"

    while IFS= read -r line
    do
        block="${line%=*}"
        cooldown="${line#*=}"

        "${bin_dir}"/run_block.sh -b "${block}.sh" -c "${cooldown}" -p "${protocol}"
    done <<EOF
$blocks
EOF

)


send_header() (
    header='{"version": 1,"click_events": true}'

    echo "${header}"

    # Endless array
    echo "["
    echo "[]"
)


format() (
    out="${1}"
    protocol="${2}"

    if [ "${protocol}" = "plain" ]; then
        printf '%s' "${out}" | tr '\n' '|' | sed 's/|/ | /g'
    else
        # json
        printf ',[%s]' "${out}" | tr '\n' ','
    fi
)


listen() (
    while read -r line
    do
        # FIXME: should actually parse the json
        block_name=$(echo "${line}" | awk '{print $3}' | tr -d '",')
        block_dir="${block_name%.*}"

        block_file="${blocks_dir}/${block_dir}/${block_name}"

        if [ -f "${block_file}" ]; then
            sh -c -- "${block_file} --click&"
        fi
    done
)


get_blocks() (
    main_config_file="${bin_dir}/../config"

    if [ ! -f "${main_config_file}" ]; then
        echo "Couldn't find config file: '${main_config_file}'" 1>&2
        exit 1
    fi

    while read -r line
    do
        # Skip blank lines and comments
        if [ -z "${line}" ] || is_comment "${line}" ; then
            continue
        fi

        block="${line%=*}"
        cooldown="${line#*=}"

        # Default cooldown is an hour
        if ! is_integer "${cooldown}"; then
            cooldown="3600"
        fi

        echo "${block}=${cooldown}"
    done < "${main_config_file}"
)


usage() (
    printf "%s\n" "Usage: ${0} [-p <json|plain>] [-t <terminal>]

options:
-b      the browser blocks should use, if they need one, on \"click\" events. Defaults to \"qutebrowser\".
-p      protocol (\"json\" or \"plain\"). Defaults to \"json\".
-t      the terminal blocks should use, if they need one, on \"click\" events. Defaults to \"foot\"." 1>&2

    exit 1
)


argparse() (
    browser="qutebrowser"
    protocol="json"
    terminal="foot"

    while getopts ':b:p:t:' opt; do
        case $opt in
            b)
                browser="${OPTARG}"
                ;;
            p)
                protocol="${OPTARG}"

                [ "${protocol}" = "json" ] \
                    || [ "${protocol}" = "plain" ] \
                    || usage
                ;;
            t)
                terminal="${OPTARG}"
                ;;
            *)
                usage
                ;;
        esac
    done

    shift "$((OPTIND - 1))"

    main "${protocol}" "${terminal}" "${browser}"
)


main() (
    protocol="${1}"
    terminal="${2}"
    browser="${3}"

    export SHBR_TERM="${terminal}"
    export SHBR_BROWSER="${browser}"

    if [ "${protocol}" = "json" ]; then
        send_header
    fi

    blocks=$(get_blocks)

    (while true
    do
        out=$(run "${blocks}" "${protocol}")

        format "${out}" "${protocol}"

        sleep 1
    done)&

    listen
)


argparse "${@}"

